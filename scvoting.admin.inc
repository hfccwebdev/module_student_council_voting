<?php

/**
 * @file
 * Administration pages for Student Council Voting.
 *
 * @see scvoting.module
 */

/**
 * Page callback for the administration page.
 */
function scvoting_admin_page() {
  drupal_set_breadcrumb(array(l(t('Home'), '<front>'), l(t('Administration'), 'admin'), l(t('Structure'), 'admin/config/hfc')));
  $output = array();
  $output[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Student Council Candidates'),
    '#suffix' => '</h3>',
  );

  $result = db_query("SELECT id, name FROM {stu_council_candidates} ORDER BY name")->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $operations = user_access('administer scvoting')
      ? l(t('edit'), "student-council/candidates/{$item->id}/edit") . ' ' . l(t('delete'), "student-council/candidates/{$item->id}/delete")
      : NULL;
    $rows[] = array(
      l(check_plain($item->name), "student-council/candidates/{$item->id}"),
      $operations,
    );
  }
  $output[] = array(
    '#theme' => 'table',
    '#header' => array(t('Name'), t('Operations')),
    '#rows' => $rows,
    '#empty' => t('No candidates found.'),
  );

  return $output;
}

/**
 * Page callback for administrative settings.
 */
function scvoting_admin_settings_form($form, &$form_state) {
  $form = array();
  $form[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Student Council Voting Settings'),
    '#suffix' => '</h3>',
  );
  $form['scvoting_max_votes'] = array(
    '#type' => 'textfield',
    '#title' => t('Votes per user'),
    '#size' => 3,
    '#description' => t('The maximum number of candidates a single voter may select.'),
    '#default_value' => variable_get('scvoting_max_votes', NULL),
  );
  $form['scvoting_active_flag'] = array(
    '#type' => 'radios',
    '#title' => t('Activate voting'),
    '#options' => array(1 => t('Active'), 0 => t('Inactive')),
    '#description' => t('Sets whether the voting form is enabled or not.'),
    '#default_value' => variable_get('scvoting_active_flag', 1),
  );
  $form[] = array(
    '#title' => t('Maintenance'),
    '#theme' => 'item_list',
    '#items' => array(
      l(t('Delete all candidates'), 'admin/config/hfc/stu-council-voting/delete-all-candidates'),
      l(t('Delete all votes'), 'admin/config/hfc/stu-council-voting/delete-all-votes'),
    ),
  );
  return system_settings_form($form);
}

/**
 * Page callback for voting results.
 */
function scvoting_admin_results_page() {
  $output = array();
  $output[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Student Council Voting Results'),
    '#suffix' => '</h3>',
  );

  $result = db_query('SELECT v.candidate_id, c.name, COUNT(v.candidate_id) AS votes FROM {stu_council_votes} v LEFT JOIN {stu_council_candidates} c ON v.candidate_id = c.id GROUP BY candidate_id ORDER BY votes DESC')->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $rows[] = array(
      l(check_plain($item->name), 'student-council/candidates/' . $item->candidate_id),
      $item->votes,
    );
  }
  $output[] = array(
    '#theme' => 'table',
    '#header' => array(t('Name'), t('Votes')),
    '#rows' => $rows,
    '#empty' => t('No results found.'),
  );

  $result = db_query('SELECT COUNT(*) AS voters FROM {stu_council_voters}')->fetchAssoc();
  // dpm($result, 'voters');

  $output[] = array(
    '#prefix' => '<p>',
    '#markup' => t('A total of %voters voters have participated in this election.', array('%voters' => $result['voters'])),
    '#suffix' => '</p>',
  );

  return $output;
}

/**
 * Confirmation form to delete all candidates.
 */
function scvoting_admin_delete_all_candidates($form, &$form_state) {
  drupal_set_title('Are you sure you want to delete all candidates?');
  $form = array();
  $form[] = array(
    '#prefix' => '<div><strong>',
    '#markup' => t('Warning! This operation destroys data! This action cannot be undone.'),
    '#suffix' => '</strong></div>',
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/hfc/stu-council-voting',
  );

  return $form;
}

/**
 * Delete all candidates.
 */
function scvoting_admin_delete_all_candidates_submit($form, &$form_state) {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'stu_council_candidates')->execute();

  if (isset($result['stu_council_candidates'])) {
    $ids = array_keys($result['stu_council_candidates']);
    scvoting_candidate_delete_multiple($ids);
    drupal_set_message(t('All candidates have been deleted.'));
  }
  else {
    drupal_set_message(t('No candidates found to delete.'));
  }
  $form_state['redirect'] = 'admin/config/hfc/stu-council-voting';
}

/**
 * Confirmation form to delete all votes.
 */
function scvoting_admin_delete_all_votes($form, &$form_state) {
  drupal_set_title('Are you sure you want to delete all votes?');
  $form = array();
  $form[] = array(
    '#prefix' => '<div><strong>',
    '#markup' => t('Warning! This operation destroys data! This action cannot be undone.'),
    '#suffix' => '</strong></div>',
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/hfc/stu-council-voting',
  );

  return $form;
}

/**
 * Delete all votes.
 */
function scvoting_admin_delete_all_votes_submit($form, &$form_state) {

  $votes = db_delete('stu_council_votes')->execute();
  $voters = db_delete('stu_council_voters')->execute();
  drupal_set_message(t('Removed %votes votes by %voters voters.', array('%votes' => $votes, '%voters' => $voters)), 'warning');
  drupal_set_message(t('All votes deleted.'));
  $form_state['redirect'] = 'admin/config/hfc/stu-council-voting';
}
