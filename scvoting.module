<?php

/**
 * @file
 * Student Council Voting.
 *
 * @ingroup hfcc_modules
 * @see scvoting.install
 */

/**
 * Implements hook_permission().
 */
function scvoting_permission() {
  return array(
    'administer scvoting' => array(
      'title' => t('Administer voting'),
      'description' => t('Perform administration tasks for Student Council voting.'),
    ),
    'scvoting vote' => array(
      'title' => t('Cast votes'),
      'description' => t('Vote for a Student Council candidate.'),
    ),
    'scvoting candidates' => array(
      'title' => t('View candidates'),
      'description' => t('View Student Council candidates.'),
    ),
    'scvoting view' => array(
      'title' => t('View results'),
      'description' => t('View Student Council voting results.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function scvoting_menu() {
  return array(
    'admin/config/hfc/stu-council-voting' => array(
      'title' => 'Student council voting',
      'description' => 'Administer student council candidates and voting',
      'page callback' => 'scvoting_admin_page',
      'access arguments' => array('scvoting view'),
      'type' => MENU_NORMAL_ITEM,
      'file' => 'scvoting.admin.inc',
    ),
    'admin/config/hfc/stu-council-voting/candidates' => array(
      'title' => 'Candidates',
      'weight' => -10,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    ),
    'admin/config/hfc/stu-council-voting/candidates/add' => array(
      'title' => 'Add a candidate',
      'page callback' => 'scvoting_candidate_add',
      'access callback' => 'entity_access',
      'access arguments' => array('create', 'stu_council_candidates'),
      'type' => MENU_LOCAL_ACTION,
      'file' => 'scvoting.pages.inc',
    ),
    'admin/config/hfc/stu-council-voting/settings' => array(
      'title' => 'Settings',
      'description' => 'Administer student council voting settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('scvoting_admin_settings_form'),
      'access arguments' => array('administer scvoting'),
      'weight' => 14,
      'type' => MENU_LOCAL_TASK,
      'file' => 'scvoting.admin.inc',
    ),
    'admin/config/hfc/stu-council-voting/results' => array(
      'title' => 'Results',
      'description' => 'Administer student council voting settings',
      'page callback' => 'scvoting_admin_results_page',
      'access arguments' => array('scvoting view'),
      'weight' => 15,
      'type' => MENU_LOCAL_TASK,
      'file' => 'scvoting.admin.inc',
    ),
    'admin/config/hfc/stu-council-voting/delete-all-candidates' => array(
      'title' => 'Delete all candidates',
      'description' => 'Remove all candidates from the system',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('scvoting_admin_delete_all_candidates'),
      'access arguments' => array('administer scvoting'),
      'type' => MENU_CALLBACK,
      'file' => 'scvoting.admin.inc',
    ),
    'admin/config/hfc/stu-council-voting/delete-all-votes' => array(
      'title' => 'Delete all votes',
      'description' => 'Remove all votes from the system',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('scvoting_admin_delete_all_votes'),
      'access arguments' => array('administer scvoting'),
      'type' => MENU_CALLBACK,
      'file' => 'scvoting.admin.inc',
    ),
    'student-council/candidates' => array(
      'title' => t('Student Council Candidates'),
      'page callback' => 'scvoting_candidates_page',
      'access callback' => 'entity_access',
      'access arguments' => array('view', 'stu_council_candidates', 2),
      'type' => MENU_CALLBACK,
      'file' => 'scvoting.pages.inc',
    ),
    'student-council/candidates/%scvoting_candidate' => array(
      'title callback' => 'entity_label',
      'title arguments' => array('stu_council_candidates', 2),
      'description' => t('Display details for an individual candidate.'),
      'page callback' => 'scvoting_candidate_page',
      'page arguments' => array(2),
      'access callback' => 'entity_access',
      'access arguments' => array('view', 'stu_council_candidates', 2),
      'type' => MENU_CALLBACK,
      'file' => 'scvoting.pages.inc',
    ),
    'student-council/candidates/%scvoting_candidate/view' => array(
      'title' => 'View',
      'weight' => -10,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    ),
    'student-council/candidates/%scvoting_candidate/edit' => array(
      'title' => 'Edit',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('scvoting_candidate_form', 2),
      'access callback' => 'entity_access',
      'access arguments' => array('update', 'stu_council_candidates', 2),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'scvoting.pages.inc',
    ),
    'student-council/candidates/%scvoting_candidate/delete' => array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('scvoting_candidate_delete', 2),
      'access callback' => 'entity_access',
      'access arguments' => array('delete', 'stu_council_candidates', 2),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'file' => 'scvoting.pages.inc',
    ),
    'student-council/vote' => array(
      'title' => 'Student Council Elections',
      'page callback' => 'scvoting_ballot_page',
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
      'file' => 'scvoting.pages.inc',
    ),
  );
}

/**
 * Implements hook_admin_paths().
 */
function scvoting_admin_paths() {
  return [
    'student-council/candidates/*/edit' => TRUE,
  ];
}

/**
 * Implements hook_entity_info().
 *
 * @see http://www.istos.it/blog/drupal-entities/drupal-entities-part-3-programming-hello-drupal-entity
 * @see http://drupal.org/node/1026420
 */
function scvoting_entity_info() {
  return array(
    'stu_council_candidates' => array(
      'label' => t('Student Council Candidates'),
      'entity class' => 'StuCouncilCandidates',
      'controller class' => 'StuCouncilCandidatesController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'stu_council_candidates',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'scvoting_access',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'id',
      ),
      'static cache' => TRUE,
      'bundles' => array(
        'stu_council_candidates' => array(
          'label' => 'Student Council Candidates',
          'admin' => array(
            'path' => 'admin/config/hfc/stu-council-voting',
            'access arguments' => array('administer scvoting'),
          ),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full display'),
          'custom settings' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * Access callback for scvoting.
 *
 * Arguments for this function are defined by entity_access().
 * @see entity_access()
 *
 */
function scvoting_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {
  switch ($op) {
    case 'view':
      return user_access('scvoting vote') || user_access('scvoting view') || user_access('scvoting candidates') || user_access('administer scvoting');
    case 'create':
    case 'update':
    case 'delete':
      return user_access('administer scvoting');
  }
}

/**
 * Auto-load function for %scvoting_candidate menu wildcard.
 */
function scvoting_candidate_load($id) {
  $candidate = entity_load('stu_council_candidates', array($id));
  return $candidate ? reset($candidate) : FALSE;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function scvoting_entity_property_info_alter(&$info) {
  $properties = &$info['stu_council_candidates']['properties'];
  $properties['id'] = array(
    'label' => t("Candidate ID"),
    'type' => 'text',
    'schema field' => 'id',
    'description' => t("The unique ID for this record."),
  );
  $properties['name'] = array(
    'label' => t('Name'),
    'type' => 'text',
    'schema field' => 'name',
    'description' => t("The candidate's full name."),
  );
}

/**
 * Deletes multiple candidates.
 *
 * Using this because entity_delete() leaves errors in its wake.
 * @see node_delete_multiple()
 */
function scvoting_candidate_delete_multiple($ids) {
  $transaction = db_transaction();
  if (!empty($ids)) {
    $ids = is_array($ids) ? $ids : array($ids);
    $entities = entity_load('stu_council_candidates', $ids);
    try {
      foreach ($entities as $id => $item) {
        field_attach_delete('stu_council_candidates', $item);
      }
      db_delete('stu_council_candidates')->condition('id', $ids, 'IN')->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('scvoting', $e);
      throw $e;
    }
    entity_get_controller('stu_council_candidates')->resetCache();
  }
}
