Student Council Voting 1.x for Drupal 7.x
=========================================

Created by Micah Webner <micah@hfcc.edu> for Henry Ford Community College

This Drupal module sets up a system for collecting and tallying student council votes, with the following features:

* Candidates are fieldable entities.
* Voters must be registered site users and are only allowed to vote once.
* Votes are registered anonymously, although not certifiably so because it could be possible to determine who voted in what order.

## Installation

Student Council Voting can be installed like any other Drupal module -- place it in
the sites/all/modules/custom directory for your site and enable it through the admin interface.
