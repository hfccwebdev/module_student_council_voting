<?php

/**
 * @file
 * Interface and Controller for StuCouncilCandidates entities.
 */

/**
 * StuCouncilCandidate class.
 */
class StuCouncilCandidates extends Entity {
  protected function defaultLabel() {
    return !empty($this->name) ? check_plain($this->name) : NULL;
  }
  protected function defaultUri() {
    return array('path' => 'student-council/candidates/' . $this->identifier());
  }
}

/**
 * StuCouncilCandidatesController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few
 * customizations to create, update, and delete methods.
 */
class StuCouncilCandidatesController extends EntityAPIController {

  /**
   * Create and return a new entity.
   */
  public function create(array $values = array()) {
    $values += array(
      'id' => 0,
      'name' => NULL,
    );
    return parent::create($values);
  }

}
