<?php

/**
 * @file
 * Contains install and update functions for Student Council Voting.
 *
 * @see scvoting.module
 */

/**
 * Implements hook_schema().
 */
function scvoting_schema() {
  $schema = array();

  $schema['stu_council_candidates'] = array(
    'description' => 'Contains base entity information for candidates.',
    'fields' => array(
      'id' => array(
        'description' => "Unique identifier for this entity.",
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => "Stores the candidate's name.",
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name')
    ),
  );

  $schema['stu_council_voters'] = array(
    'description' => "Contains a list of users who have already voted.",
    'fields' => array(
      'uid' => array(
        'description' => "Stores the voter's user id.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'stamp' => array(
        'description' => "Stores the time this user's vote was cast.",
        'type' => 'int',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid'),
  );

  $schema['stu_council_votes'] = array(
    'description' => "Stores each vote that has been cast.",
    'fields' => array(
      'id' => array(
        'description' => "The unique identifier for this vote.",
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'candidate_id' => array(
        'description' => "Stores the selected candidate ID for this vote.",
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'candidate_id' => array('candidate_id'),
    ),
  );

  return $schema;
}
