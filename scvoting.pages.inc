<?php

/**
 * @file
 * Page callbacks for Student Council Voting.
 *
 * @see scvoting.module
 */

/**
 * Page callback to display candidates list.
 */
function scvoting_candidates_page() {
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('Student Council Voting'), 'student-council/vote'),
  ));
  $result = db_query("SELECT id, name FROM {stu_council_candidates} ORDER BY name")->fetchAll();
  $rows = array();
  foreach ($result as $item) {
    $rows[] = array(l(check_plain($item->name), "student-council/candidates/{$item->id}"));
  }
  if (!empty($rows)) {
    return array(
      '#theme' => 'item_list',
      '#items' => $rows,
    );
  }
  else {
    return array(
      '#markup' => t('No candidates found to display.'),
    );
  }
}

/**
 * Page callback to display a candidate.
 */
function scvoting_candidate_page($candidate) {
  drupal_set_title($candidate->label());
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('Student Council Voting'), 'student-council/vote'),
    l(t('Candidates'), 'student-council/candidates'),
  ));
  return $candidate->view();
}

/**
 * Add a new candidate.
 */
function scvoting_candidate_add() {
  $entity = entity_create('stu_council_candidates', array());
  return drupal_get_form('scvoting_candidate_form', $entity);
}

/**
 * Edit form for candidate.
 */
function scvoting_candidate_form($form, &$form_state, $entity) {
  // dpm($entity, 'entity');
  drupal_set_title($entity->label());

  $form = array();

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t("The candidate's name (first and last.)"),
    '#default_value' => !empty($entity->name) ? $entity->name : NULL,
    '#required' => TRUE,
  );

  field_attach_form('stu_council_candidates', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Validate the entity edit form.
 *
 * We don't have any native fields to validate, but we want to
 * allow Field API to do any validation that it requires.
 */
function scvoting_candidate_form_validate($form, &$form_state) {
  $values = (object) $form_state['values'];
  field_attach_form_validate('stu_council_candidates', $values, $form, $form_state);
}

/**
 * Entity editing form submit.
 */
function scvoting_candidate_form_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  $entity->name = $form_state['values']['name'];
  field_attach_submit('stu_council_candidates', $entity, $form, $form_state);
  entity_save('stu_council_candidates', $entity);
  $form_state['redirect'] = 'student-council/candidates/' . $entity->identifier();
}

/**
 * Prompt for confirmation before deleting a candidate.
 */
function scvoting_candidate_delete($form, &$form_state, $entity) {
  drupal_set_title(t('Are you sure you want to delete candidate %name?', array('%name' => $entity->name)), PASS_THROUGH);
  $form = array();
  $form[] = array(
    '#prefix' => '<div><strong>',
    '#markup' => t('Warning! This operation destroys data! This action cannot be undone.'),
    '#suffix' => '</strong></div>',
  );

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/hfc/stu-council-voting',
  );

  return $form;
}

/**
 * Delete candidate form submit.
 */
function scvoting_candidate_delete_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  scvoting_candidate_delete_multiple(array($entity->id));
  drupal_set_message(t('Candidate %id - %name deleted.', array('%id' => $entity->id, '%name' => $entity->name)));
  $form_state['redirect'] = 'admin/config/hfc/stu-council-voting';
}

/**
 * Page callback for voting.
 *
 * We need to do this to make sure we can vote before displaying the form.
 */
function scvoting_ballot_page() {
  global $user;

  if (variable_get('scvoting_active_flag', 1) == 0) {
    return array(
      '#prefix' => '<p><strong>',
      '#markup' => t('This election is now closed.'),
      '#suffix' => '</strong></p>',
    );
  }
  if ($user->uid == 0) {
    return array(
      array(
        '#prefix' => '<div><strong>',
        '#markup' => t('Please log in before voting.'),
        '#suffix' => '</strong></div>',
      ),
      // For some reason, this does not work with NetIQ Access Manager.
      // Place the user login block on student-council/voting page instead.
      // drupal_get_form('user_login_block'),
    );
  }
  elseif (!user_access('scvoting vote')) {
    return array(
      '#prefix' => '<strong>',
      '#markup' => t('User %name does not have access to vote at this time.', array('%name' => $user->name)),
      '#suffix' => '</strong>',
    );
  }
  elseif ($result = db_query("SELECT uid, stamp FROM {stu_council_voters} WHERE uid = :uid", array(':uid' => $user->uid))->fetchAssoc()) {
    return array(
      '#prefix' => '<strong>',
      '#markup' => t('User %name has already voted on %stamp', array('%name' => $user->name, '%stamp' => format_date($result['stamp']))),
      '#suffix' => '</strong>',
    );
  }
  else {
    return drupal_get_form('scvoting_ballot_form');
  }
}

/**
 * Ballot form.
 */
function scvoting_ballot_form($form, &$form_state) {
  global $user;
  $form = array();

  if ($result = db_query("SELECT id FROM {stu_council_candidates} ORDER BY name")->fetchAll()) {

    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $user->uid,
    );

    $ids = array();
    foreach ($result as $item) {
      $ids[] = $item->id;
    }
    $entities = entity_load('stu_council_candidates', $ids);
    // dpm($entities, 'entities');
    $options = array();
    foreach ($entities as $entity) {
      $options[$entity->id] = l($entity->name, 'student-council/candidates/' . $entity->id);
    }
    $form['selections'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select up to @max candidates', array('@max' => variable_get('scvoting_max_votes', 2))),
      '#options' => $options,
      '#required' => TRUE,
      '#description' => t('You may click the names to view more information about each candidate.'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Vote!'),
      '#weight' => 100,
    );

  }
  else {
    $form[] = array('#markup' => 'No candidates found.');
  }
  return $form;
}

/**
 * Validation for ballot form.
 */
function scvoting_ballot_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $max_votes = variable_get('scvoting_max_votes', 2);
  $votes = 0;
  foreach ($values['selections'] as $vote) {
    if ($vote) {
      $votes++;
    }
  }
  if ($votes > $max_votes) {
    form_set_error('selections', t('Please select only %x candidates.', array('%x' => $max_votes)));
  }
}

/**
 * Submission handler for ballot form.
 */
function scvoting_ballot_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  // dpm ($values, 'values');
  $vote_id = NULL;
  foreach ($values['selections'] as $candidate_id => $vote) {
    if ($vote) {
      $vote_id = db_insert('stu_council_votes')
        ->fields(array('candidate_id' => $candidate_id))
        ->execute();
      drupal_set_message(t('Vote #@id registered.', array('@id' => $vote_id)));
    }
  }
  if ($vote_id) {
    $voter_id = db_insert('stu_council_voters')
      ->fields(array('uid' => $values['uid'], 'stamp' => time()))
      ->execute();
    drupal_set_message(t('Your votes have been registered. Thank you for voting.'));
  }
}
